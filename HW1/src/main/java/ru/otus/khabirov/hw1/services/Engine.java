package ru.otus.khabirov.hw1.services;

/**
 * the engine of interview
 */
public interface Engine {

    /**
     * start interview
     */
    void start();
}
