package ru.otus.khabirov.hw1.services;

import ru.otus.khabirov.hw1.domain.QuestionDo;

/**
 * operations with QuestionDo
 */
public interface QuestionService {

    /**
     * get QuestionDo by id
     * @param id id
     * @return QuestionDo
     */
    QuestionDo get(Long id);
}
