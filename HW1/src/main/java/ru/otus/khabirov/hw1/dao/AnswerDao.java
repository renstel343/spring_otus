package ru.otus.khabirov.hw1.dao;

import ru.otus.khabirov.hw1.domain.AnswerDo;

/**
 * access to AnswerDo
 */
public interface AnswerDao {

    /**
     * find AnswerDo by id
     * @param id id
     * @return AnswerDo
     */
    AnswerDo findById(Long id);
}
