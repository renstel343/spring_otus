package ru.otus.khabirov.hw1.services;

import ru.otus.khabirov.hw1.domain.AnswerDo;

/**
 * operations with AnswerDo
 */
public interface AnswerService {

    /**
     * get AnswerDo by id
     *
     * @param id id
     * @return AnswerDo
     */
    AnswerDo get(Long id);
}
