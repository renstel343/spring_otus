package ru.otus.khabirov.hw1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.otus.khabirov.hw1.services.Engine;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
        Engine bean = context.getBean(Engine.class);
        bean.start();
    }
}
