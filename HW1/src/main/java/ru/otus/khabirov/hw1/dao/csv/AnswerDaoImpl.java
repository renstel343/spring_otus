package ru.otus.khabirov.hw1.dao.csv;

import org.springframework.core.convert.ConversionService;
import ru.otus.khabirov.hw1.dao.AnswerDao;
import ru.otus.khabirov.hw1.dao.helper.FileFinder;
import ru.otus.khabirov.hw1.domain.AnswerDo;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * access to answers
 */
public class AnswerDaoImpl implements AnswerDao {

    private static final String PATH = "csv/answers.csv";
    private static Map<Long, SoftReference<AnswerDo>> referenceMap = new HashMap<>();

    private final ConversionService service;

    /**
     * public constructor
     * @param service converter
     */
    public AnswerDaoImpl(ConversionService service) {
        this.service = service;
    }

    /**
     * find answer by id
     * @param id id
     * @return answer
     */
    @Override
    public AnswerDo findById(Long id) {
        return Optional.ofNullable(referenceMap.get(id))
                .map(SoftReference::get)
                .orElse(service.convert(FileFinder.find(id + " ", PATH)
                        .orElse(null), AnswerDo.class));
    }
}
