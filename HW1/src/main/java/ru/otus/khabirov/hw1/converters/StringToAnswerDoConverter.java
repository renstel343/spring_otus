package ru.otus.khabirov.hw1.converters;

import org.springframework.core.convert.converter.Converter;
import ru.otus.khabirov.hw1.domain.AnswerDo;

/**
 * Convert text to AnswerDo
 */
public class StringToAnswerDoConverter implements Converter<String, AnswerDo> {

    private static final int ID_INDEX = 0;
    private static final int QUESTION_ID_INDEX = 1;
    private static final int ANSWER_INDEX = 2;
    private static final int SCORE_INDEX = 3;

    /**
     * Convert text to AnswerDo
     *
     * @param source text
     * @return AnswerDo
     */
    @Override
    public AnswerDo convert(final String source) {
        String[] split = source.split(",");
        return AnswerDo.builder()
                .id(Long.parseLong(split[ID_INDEX].trim()))
                .questionId(Long.parseLong(split[QUESTION_ID_INDEX].trim()))
                .answer(split[ANSWER_INDEX].trim())
                .score(Short.parseShort(split[SCORE_INDEX]))
                .build();
    }
}
