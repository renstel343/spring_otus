package ru.otus.khabirov.hw1.services;

/**
 * interaction with external system
 */
public interface Cooperation {

    /**
     * send message to external system
     * @param text
     */
    void print(String text);

    /**
     * get text from external system
     * @return input data
     */
    String input();

    /**
     * close resources
     */
    void close();
}
