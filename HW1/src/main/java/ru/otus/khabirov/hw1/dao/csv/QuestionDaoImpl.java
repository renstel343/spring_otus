package ru.otus.khabirov.hw1.dao.csv;

import org.springframework.core.convert.ConversionService;
import ru.otus.khabirov.hw1.dao.QuestionDao;
import ru.otus.khabirov.hw1.dao.helper.FileFinder;
import ru.otus.khabirov.hw1.domain.QuestionDo;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * access to questions
 */
public class QuestionDaoImpl implements QuestionDao {

    private static final String PATH = "csv/questions.csv";
    private static Map<Long, SoftReference<QuestionDo>> referenceMap = new HashMap<>();

    private final ConversionService service;

    /**
     * public constructor
     * @param service converter
     */
    public QuestionDaoImpl(ConversionService service) {
        this.service = service;
    }

    /**
     * find question by id
     * @param id id
     * @return question
     */
    @Override
    public QuestionDo findById(Long id) {
        return Optional.ofNullable(referenceMap.get(id))
                .map(SoftReference::get)
                .orElse(service.convert(FileFinder.find(id + " ", PATH)
                        .orElse(null), QuestionDo.class));
    }
}
