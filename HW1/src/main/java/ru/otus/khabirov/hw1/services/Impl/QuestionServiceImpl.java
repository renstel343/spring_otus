package ru.otus.khabirov.hw1.services.Impl;

import ru.otus.khabirov.hw1.dao.QuestionDao;
import ru.otus.khabirov.hw1.domain.QuestionDo;
import ru.otus.khabirov.hw1.services.QuestionService;

/**
 * operations with QuestionDo
 */
public class QuestionServiceImpl implements QuestionService {

    private final QuestionDao dao;

    /**
     * public constructor
     * @param dao access to repository
     */
    public QuestionServiceImpl(QuestionDao dao) {
        this.dao = dao;
    }

    /**
     * get QuestionDo by id
     *
     * @param id id
     * @return QuestionDo
     */
    @Override
    public QuestionDo get(Long id) {
        return dao.findById(id);
    }
}
