package ru.otus.khabirov.hw1.services.Impl;

import ru.otus.khabirov.hw1.domain.AnswerDo;
import ru.otus.khabirov.hw1.services.AnswerService;
import ru.otus.khabirov.hw1.services.Cooperation;
import ru.otus.khabirov.hw1.services.Engine;
import ru.otus.khabirov.hw1.services.QuestionService;

import java.text.MessageFormat;

public class EngineImpl implements Engine {

    private final Cooperation cooperation;
    private final QuestionService questionService;
    private final AnswerService answerService;


    public EngineImpl(Cooperation cooperation, QuestionService questionService, AnswerService answerService) {
        this.cooperation = cooperation;
        this.questionService = questionService;
        this.answerService = answerService;
    }

    @Override
    public void start() {
        cooperation.print("Какова ваша Фамилия? Имя?");
        cooperation.input();

        int result = 0;
        for (long i = 0; i < 5; i++) {
            cooperation.print(questionService.get(i).getQuestion());
            String input = cooperation.input();
            AnswerDo answerDo = answerService.get(i);
            if (answerDo.getAnswer().equals(input)) {
                result += answerDo.getScore();
            }
        }

        cooperation.print(MessageFormat.format("Ваш результат {0}", result));
        cooperation.close();
    }
}
