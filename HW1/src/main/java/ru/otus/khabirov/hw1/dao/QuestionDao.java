package ru.otus.khabirov.hw1.dao;

import ru.otus.khabirov.hw1.domain.QuestionDo;

/**
 * access to QuestionDo
 */
public interface QuestionDao {

    /**
     * find QuestionDo by id
     * @param id id
     * @return QuestionDo
     */
    QuestionDo findById(Long id);
}
