package ru.otus.khabirov.hw1.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AnswerDo {

    private Long id;
    private Long questionId;
    private String answer;
    private Short score;
}
