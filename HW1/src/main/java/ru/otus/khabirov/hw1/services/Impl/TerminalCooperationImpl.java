package ru.otus.khabirov.hw1.services.Impl;

import lombok.SneakyThrows;
import ru.otus.khabirov.hw1.services.Cooperation;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * interaction with terminal
 */
public class TerminalCooperationImpl implements Cooperation {

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * send message to terminal
     * @param text
     */
    @Override
    public void print(String text) {
        System.out.println(text);
    }

    /**
     * get text from terminal
     * @return input data
     */
    @Override
    @SneakyThrows
    public String input() {
        return reader.readLine();
    }

    /**
     * close resources
     */
    @Override
    @SneakyThrows
    public void close() {
        reader.close();
    }
}
