package ru.otus.khabirov.hw1.converters;

import org.springframework.core.convert.converter.Converter;
import ru.otus.khabirov.hw1.domain.QuestionDo;

/**
 * Convert text to QuestionDo
 */
public class StringToQuestionDoConverter implements Converter<String, QuestionDo> {

    private static final int ID_INDEX = 0;
    private static final int QUESTION_INDEX = 1;

    /**
     * Convert text to QuestionDo
     *
     * @param source text
     * @return QuestionDo
     */
    @Override
    public QuestionDo convert(String source) {
        String[] split = source.split(",");
        return QuestionDo.builder()
                .id(Long.parseLong(split[ID_INDEX].trim()))
                .question(split[QUESTION_INDEX].trim())
                .build();
    }
}
