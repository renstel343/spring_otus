package ru.otus.khabirov.hw1.dao.helper;

import lombok.Cleanup;
import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * find line by id in text
 */
public class FileFinder {

    @SneakyThrows
    public static Optional<String> find(String id, String filePath) {
        @Cleanup InputStreamReader in = new InputStreamReader(ClassLoader.getSystemResourceAsStream(filePath));
        @Cleanup Stream<String> stream = new BufferedReader(in).lines();
        return stream.filter(line -> line.startsWith(id)).findFirst();
    }
}
