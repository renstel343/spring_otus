package ru.otus.khabirov.hw1.services.Impl;

import ru.otus.khabirov.hw1.dao.AnswerDao;
import ru.otus.khabirov.hw1.domain.AnswerDo;
import ru.otus.khabirov.hw1.services.AnswerService;

/**
 * operations with AnswerDo
 */
public class AnswerServiceImpl implements AnswerService {

    private final AnswerDao dao;

    /**
     * public constructor
     * @param dao access to repository
     */
    public AnswerServiceImpl(AnswerDao dao) {
        this.dao = dao;
    }

    /**
     * get AnswerDo by id
     *
     * @param id id
     * @return AnswerDo
     */
    @Override
    public AnswerDo get(Long id) {
        return dao.findById(id);
    }
}
