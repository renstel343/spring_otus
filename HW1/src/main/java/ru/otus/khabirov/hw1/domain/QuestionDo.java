package ru.otus.khabirov.hw1.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QuestionDo {

    private Long id;
    private String question;
}
