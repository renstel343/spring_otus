package ru.otus.khabirov.hw1.services.Impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.khabirov.hw1.dao.AnswerDao;
import ru.otus.khabirov.hw1.domain.AnswerDo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AnswerServiceImplTest {

    private static final String ANSWER = "answer";
    @Mock
    private AnswerDao dao;
    private AnswerServiceImpl service;
    @Captor
    private ArgumentCaptor<Long> captor;

    @BeforeEach
    void setUp() {
        service = new AnswerServiceImpl(dao);
        AnswerDo answer = AnswerDo.builder()
                .id(1l)
                .questionId(1l)
                .answer(ANSWER)
                .score((short) 1)
                .build();
        when(service.get(anyLong())).thenReturn(answer);
    }

    @Test()
    void get() {
        AnswerDo result = service.get(captor.capture());
        assertNotNull(result);
        assertEquals(ANSWER, result.getAnswer());
        verify(dao).findById(anyLong());
    }
}