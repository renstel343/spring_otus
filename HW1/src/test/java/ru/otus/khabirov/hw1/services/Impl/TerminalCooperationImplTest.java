package ru.otus.khabirov.hw1.services.Impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

@ExtendWith(MockitoExtension.class)
class TerminalCooperationImplTest {

    @Mock
    private TerminalCooperationImpl service;
    private ByteArrayOutputStream outContent;
    private ByteArrayInputStream inContent;
    private InputStream in;
    private PrintStream out;


    @BeforeEach
    void setUp() {
        in = System.in;
        out = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        inContent = new ByteArrayInputStream("text".getBytes());
        System.setIn(inContent);
        service = new TerminalCooperationImpl();

    }

    @Test
    void print() {
        service.print("text");
        Assertions.assertEquals("text", outContent.toString().trim());

    }

    @Test
    void input() {
        String input = service.input();
        Assertions.assertEquals("text", input);
    }

    @AfterEach
    void tearDown() {
        System.setIn(in);
        System.setOut(out);
    }
}