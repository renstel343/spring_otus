package ru.otus.khabirov.hw1.services.Impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.khabirov.hw1.dao.QuestionDao;
import ru.otus.khabirov.hw1.domain.QuestionDo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class QuestionServiceImplTest {

    private static final String QUESTION = "question";
    @Mock
    private QuestionDao dao;
    private QuestionServiceImpl service;
    @Captor
    private ArgumentCaptor<Long> captor;

    @BeforeEach
    void setUp() {
        service = new QuestionServiceImpl(dao);
        QuestionDo question = QuestionDo.builder()
                .id(1l)
                .question(QUESTION)
                .build();
        when(service.get(anyLong())).thenReturn(question);
    }

    @Test
    void get() {
        QuestionDo result = service.get(captor.capture());
        assertNotNull(result);
        assertEquals(QUESTION, result.getQuestion());
        verify(dao).findById(anyLong());
    }
}